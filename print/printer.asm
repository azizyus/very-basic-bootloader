

hexTable db "0123456789ABCDEF",0
test_string db "this",0


printer:
      mov al,[bx] ;read byte from memory specified address via bx, kinda pointer
      inc bx ; ++ to bx so i can read next byte of string to print
      call printChar ;print character in al register
	  cmp dh,[bx]; 0x24 == $ its something from 8080 when i was printing in CP/M
      jnz printer
;	  cmp = sub em and change flags by result, many diagnostic test uses it in 8080
;	  jz output ; if z flag is 0(zero) then go to specified label/address
      ret ;return from function



printerInit:
    pusha
    mov dh,0x00
    call printer
    popa
    ret

printHexInit:
    pusha
    mov dh,0x00 ;determines end character of strings so print may stop
    call printerHex
    popa
    ret

printerHex:
      mov al,[bx] ;read byte from memory specified address via bx, kinda pointer
      inc bx ; ++ to bx so i can read next byte of string to print
      push bx

      call printHexLeft
      call printHexRight

      call printSpace


      pop bx
	  cmp dh,[bx]; 0x24 == $ its something from 8080 when i was printing in CP/M
      jnz printerHex

;	  cmp = sub em and change flags by result, many diagnostic test uses it in 8080
;	  jz output ; if z flag is 0(zero) then go to specified label/address
      ret ;return from function

printSpace:
    push ax
    mov al,' '
    call printChar
    pop ax
    ret

printHexLeft:
      push ax
      mov bx, hexTable
      shr al, 4 ;; shift from right
      add bx, ax
      mov al, [bx]
      call printChar ;print character in al register
      pop ax
      ret

printHexRight:
      push ax
      mov bx, hexTable
      and al, 0x0F
      add bx, ax
      mov al, [bx]
      call printChar  ;print character in al register
      pop ax
      ret


printChar:
          mov ah, 0x0e ; tells bios to i will print something
    	  int 0x10 ; interrupt
          ret