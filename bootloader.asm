;org 0x7C00 ; program starts from specified address
bits 16 ; it says we ll be work in 16bit mode, its real mode and neccessary for bootloaders so you cant define 32 or 64 bit variables



_start:
;    times 3-($-$$) DB 0x90   ; there is something called BPB, which overries mycode so thats why it wasnt working.
    ;basically im giving fake data to BPB for override
;    times 59 DB 0xAA ;these lines copy pasted from a stackoverflow topic but cant remember exactly
;    jmp start ;jump to start:
;	xor ax,ax ;ax will be equal to zero
	;----------


    call disk_store_bootdrive


    ; init registers and default values
    mov ax, 0

    mov ds, ax ;set data segment for address offsets
;    mov ax, 0
    mov es, ax
    mov ss, ax
    mov fs, ax
    mov gs, ax
    mov ss, ax


    ;configure stack
    mov bp, 0x9000
    mov sp,bp


    mov bx, MSG_REAL_MODE
    call printerInit

    call load_kernel


    ;; AFTER THIS LINES WE ARE PREPARING TO SWITCH 32BIT MODE WHICH IS CALLED PROTECTED MODE
    cli
    lgdt [gdt_descriptor]
    mov eax, cr0
    or eax, 0x1
    mov cr0, eax


    jmp CODE_SEG:start_protected_mode

    mov bx, MSG_REAL_MODE
    call printerInit

    call halt


halt: jmp $


%include "print/printer.asm"
%include "disk/disk_load.asm"



load_kernel:
    mov bx, MSG_LOAD_KERNEL
    call printerInit
    call disk_init
    call disk_load
    ret


;THOSE HAS [32 BIT] FLAG
%include "print/vga_print32.asm"
%include "gdt/gdt.asm"





bits 32

start_protected_mode:

    mov ax, DATA_SEG
    mov ds, ax
    mov es, ax
    mov fs, ax
    mov gs, ax
    mov ss, ax
;
    mov ebp, 0x90000 ; stack
    mov esp,ebp
    call BEGIN_PM



BEGIN_PM:
    mov ebx, MSG_PROT_MODE
    call print_string_pm
    call KERNEL_OFFSET
    jmp $

MSG_REAL_MODE   db "Started  in 16-bit  Real  Mode", 0
MSG_PROT_MODE   db "Successfully  landed  in 32-bit  Protected  Mode", 0
MSG_LOAD_KERNEL   db "Kernel is Loading...", 0


times 510 - ($ - $$) db 0
dw 0xAA55 ; bios looks for this data to determine end of my program



;times 256 dw 0xdade
;times 256 dw 0xface
