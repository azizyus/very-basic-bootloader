
KERNEL_OFFSET  equ 0x1000


BOOT_DRIVE: db 0
disk_error_message db "Disk Read Error!",0
disk_error_message1 db "Disk Read Error!_1",0
disk_success_message db "Disk Read Success!",0

disk_store_bootdrive:
    mov [BOOT_DRIVE],dl


disk_init:
        mov bx, KERNEL_OFFSET ; load after 0x9000
        mov dh, 15 ; load 5 sector
        mov dl, [BOOT_DRIVE]
;        call disk_load

;        mov bx, 0x9000
;        call printHexInit


disk_load:
   ;READ DISK
    push dx ;total sector i want to read and its already set in _start:

    mov ah, 0x02 ; bios read sector function
    mov al,dh
    mov ch,0x00 ; cylinder 0
    mov dh,0x00 ; select head 0
    mov cl,0x02 ; start reading from second sector, 2. sector because i want to read after bootloader sector which is actually 2

    int 0x13 ; BIOS interrupt to do actual read
    jc disk_error

    pop dx
    cmp dh, al
    jne disk_error
    ret



disk_error:
    mov bx,disk_error_message
    call printerInit
    call halt

