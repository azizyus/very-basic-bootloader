all: os-image




bootloader.o: bootloader.asm
	nasm -g -f elf32 -F dwarf -o bootloader.o bootloader.asm
	ld -melf_i386 -Ttext=0x7c00 -nostdlib --nmagic -o bootloader.elf bootloader.o
	objcopy -O binary bootloader.elf bootloader.bin




C_SOURCES = $(wildcard kernel/*.c drivers/*.c)
HEADERS = $(wildcard kernel/*.h drivers/*.h)
OBJ = ${C_SOURCES:.c=.o}

kernel.o: ${OBJ}	
	ld -melf_i386 -o kernel.elf -Ttext 0x1000 -nostdlib --nmagic $^
	objcopy -O binary kernel.elf kernel.bin


%.o : %.c
	gcc -g -m32 -lgcc -ffreestanding -c $< -o $@

os-image: bootloader.o kernel.o
	dd if=/dev/zero of=disk.img bs=8192 count=2880
	dd if=bootloader.bin of=disk.img bs=512 conv=notrunc
	dd if=kernel.bin of=disk.img bs=512 seek=5 conv=notrunc


clean: 
	rm kernel/*.o drivers/*.o build/*.bin build/*.o build/*.elf *.o *.bin *.elf disk.img
		



#objcopy -O binary build/kernel.elf build/kernel.bin
#
#
#
#dd if=/dev/zero of=build/disk.img bs=8192 count=2880
#dd if=builder/kernel.bin of=build/disk.img conv=notrunc
#dd if=build/bootloader.bin of=build/disk.img bs=512 conv=notrunc
#dd if=build/kernel.bin of=build/disk.img bs=512 seek=5 conv=notrunc
#sync
