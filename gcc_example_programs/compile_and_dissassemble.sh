gcc -ffreestanding -c $1.c -o $1.o
ld -o $1.bin -Ttext 0x0 --oformat binary $1.o
ndisasm -b 32 $1.bin > $1.dis